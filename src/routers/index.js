import * as React from "react";
import { Routes, Route } from "react-router-dom";
import {
  Anggota,
  AnggotaCalon,
  AnggotaShow,
  AnggotaUnverifiedShow,
  Attention,
  Auth,
  ComingSoon,
  Dashboard,
  Error404,
  Invitation,
  ManagemenAssets,
  ManagemenAssetsAdminCabang,
  ManagemenAssetsAdminCabangShow,
  Settings,
  Ticket,
  TicketShow,
} from "../pages";
import { Spin } from "antd";
import { LoadingOutlined } from "@ant-design/icons";

export const Router = () => {
  const [isLoading, setIsLoading] = React.useState(true);
  const isLoggedIn = localStorage.getItem("token") ? true : false;

  React.useEffect(() => {
    isLoggedIn
      ? setIsLoading(false)
      : setTimeout(() => {
          setIsLoading(false);
        }, 2000);
  }, [isLoggedIn]);

  const chekLoggedIn = (components) => {
    if (isLoggedIn) {
      return components;
    } else {
      return isLoading ? <Loading /> : <Error404 />;
    }
  };
  return (
    <>
      <Routes>
        <Route exact path="/" element={<Auth />} />
        <Route path="/attention" element={<Attention />} />
        <Route path="/dashboard" element={chekLoggedIn(<Dashboard />)} />
        <Route path="/coming-soon" element={chekLoggedIn(<ComingSoon />)} />
        <Route path="/anggota" element={chekLoggedIn(<Anggota />)} />
        <Route path="/anggota/:slug" element={chekLoggedIn(<AnggotaShow />)} />
        <Route
          path="/anggota-unverified"
          element={chekLoggedIn(<AnggotaCalon />)}
        />
        <Route
          path="/anggota-unverified/:slug"
          element={chekLoggedIn(<AnggotaUnverifiedShow />)}
        />
        <Route path="/settings" element={chekLoggedIn(<Settings />)} />
        <Route path="/invitation" element={chekLoggedIn(<Invitation />)} />
        <Route
          path="/management-assets"
          element={chekLoggedIn(<ManagemenAssets />)}
        />
        <Route
          path="/management-assets/cabang"
          element={chekLoggedIn(<ManagemenAssetsAdminCabang />)}
        />
        <Route
          path="/management-assets/cabang/:slug"
          element={chekLoggedIn(<ManagemenAssetsAdminCabangShow />)}
        />
        <Route path="/ticket" element={chekLoggedIn(<Ticket />)} />
        <Route path="/ticket/:slug" element={chekLoggedIn(<TicketShow />)} />
        <Route path="/user-management" element={chekLoggedIn(<ComingSoon />)} />
        <Route path="*" element={isLoading ? <Loading /> : <Error404 />} />
      </Routes>
    </>
  );
};

const Loading = () => {
  const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />;
  return (
    <div className="flex justify-center items-center min-h-screen">
      <div>
        <div className="flex justify-center items-center">
          <Spin indicator={antIcon} />
        </div>
        <p className="text-center mt-4 text-gray-500 lowercase tracking-widest text-xs">
          chek page
        </p>
      </div>
    </div>
  );
};
