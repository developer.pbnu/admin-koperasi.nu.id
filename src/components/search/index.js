import React from "react";
import { AiOutlineSearch } from "react-icons/ai";

export const Search = ({ onChange }) => {
  return (
    <>
      <div className="bg-white rounded-lg relative w-max px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 flex justify-between items-center">
        <AiOutlineSearch className="text-lg mr-4" />
        <input
          className="focus:outline-none focus:ring-gray-500 focus:border-gray-500 focus:z-10 sm:text-sm w-full"
          onChange={onChange}
          placeholder="Search"
        />
      </div>
    </>
  );
};
