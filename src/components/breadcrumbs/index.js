import React from "react";
import { FiChevronRight } from "react-icons/fi";
import { Link } from "react-router-dom";

export const Breadcrumbs = ({ data }) => {
  return (
    <>
      <nav className="text-gray-800 my-2">
        <div className="inline-flex">
          <div className="flex items-center">
            <Link
              to="/dashboard"
              className="text-gray-800 hover:text-orange-600 capitalize"
            >
              Dashboard
            </Link>
            <FiChevronRight className="mx-1 text-gray-800" />
          </div>
          {data.map((dt, index) => {
            return (
              <div key={index} className="flex items-center">
                <Link
                  to={dt?.to}
                  className="text-gray-800 hover:text-orange-600 capitalize"
                >
                  {dt?.title}
                </Link>
                {!dt?.end && <FiChevronRight className="mx-1 text-gray-800" />}
              </div>
            );
          })}
        </div>
      </nav>
    </>
  );
};
