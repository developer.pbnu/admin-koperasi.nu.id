import React, { useState } from "react";
import { Form, Upload, Button, message } from "antd";
import ImgCrop from "antd-img-crop";
import { fileRepository } from "../../hooks/file";
import { appConfig } from "../../configs/env";
const { Dragger } = Upload;

export const UploadFile = (props) => {
    const [loading, setLoading] = useState(false);
    const [responseFile, setResponseFile] = useState("");

    const uploadHandler = async (args) => {
        try {
            const file = args.file;
            const res = await fileRepository.api.uploadFile(file, props?.type);
            props.setValue(res.data.filename);
            setResponseFile(res.data.filename);
            setLoading(false);
            return res.data.filename;
        } catch (e) {
            message.error("Failed Upload");
        }
    };

    const beforeUpload = (file) => {
        const isJpgOrPng = file.type === "image/jpeg" || file.type === "image/png";
        if (!isJpgOrPng) {
            message.error("You can only upload JPG/PNG file!");
        }
        const isLt2M = file.size / 1024 / 1024 < 2;
        if (!isLt2M) {
            message.error("Image must smaller than 2MB!");
        }
        return isJpgOrPng && isLt2M;
    };

    return (
        <>
            <Form.Item
                label={props?.label}
                shape="circle"
                style={{
                    marginBottom: 30,
                }}
                required={true}
                name={props?.name}
                rules={props?.rules}
            >
                {loading && <p>loading</p>}
                <ImgCrop
                    rotate
                    modalTitle={props?.label}
                    aspect={props?.aspect / 2}
                    grid={true}
                >
                    <Dragger
                        showUploadList={false}
                        beforeUpload={beforeUpload}
                        maxCount={1}
                        customRequest={(args) => {
                            uploadHandler(args, props?.type);
                        }}
                        style={{ borderRadius: 10, padding: "0px 12px" }}
                    >
                        {responseFile ? (
                            <div style={{ display: "flex", justifyContent: "center" }}>
                                {console.log(`${appConfig.apiUrl}/files/${responseFile}`)}
                                <img
                                    style={{ borderRadius: 10, width: "40%" }}
                                    src={`${appConfig.apiUrl}/files/${responseFile}`}
                                    alt={props?.type}
                                />
                                <p
                                    style={{
                                        textAlign: "left",
                                        marginLeft: 8,
                                        fontSize: 12,
                                        color: "gray",
                                    }}
                                >
                                    {responseFile}
                                </p>
                            </div>
                        ) : (
                            <>
                                <Button type="dashed">Upload file *jpg/png</Button>
                            </>
                        )}
                    </Dragger>
                </ImgCrop>
            </Form.Item>
        </>
    );
};
