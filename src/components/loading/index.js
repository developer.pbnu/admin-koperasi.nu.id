import React from "react";
import Lottie from "lottie-react-web";
import loading from "../../assets/json/loading.json";

export const Loading = () => {
  return (
    <div className="z-50 fixed top-0 left-0 h-screen w-screen bg-gray-800 bg-opacity-20 flex justify-center items-center">
      <div>
        <Lottie
          width={200}
          height={200}
          options={{
            animationData: loading,
          }}
        />
      </div>
    </div>
  );
};
