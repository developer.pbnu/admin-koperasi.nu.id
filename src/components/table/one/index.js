import React from "react";
import { Table } from "antd";

// Standart table
export const TableOne = ({ loading, dataSource, columns, rowKey }) => {
  return (
    <div>
      <Table
        rowKey={rowKey}
        bordered
        loading={loading}
        dataSource={dataSource}
        columns={columns}
        pagination={
          dataSource.length > 0
            ? {
                defaultPageSize: 10,
                showSizeChanger: true,
                showTotal: (total, range) =>
                  `${range[0]}-${range[1]} of ${total} items`,
                total: dataSource.length,
                pageSizeOptions: ["10", "20", "50", "100"],
                showQuickJumper: true,
                pageSize: 10,
                position: ["bottomRight"],
                itemRender: (current, type, originalElement) => {
                  if (type === "prev") {
                    return (
                      <span className="text-gray-400 hover:text-gray-600">
                        Previous
                      </span>
                    );
                  }
                  if (type === "next") {
                    return (
                      <span className="text-gray-400 hover:text-gray-600">
                        Next
                      </span>
                    );
                  }
                  return originalElement;
                },
              }
            : false
        }
      />
    </div>
  );
};
