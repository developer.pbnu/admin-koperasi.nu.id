import { appConfig } from "../../configs/env";
import axios from "axios";
import { useMessage } from "../../global/useMessage";

const instance = axios.create({
  baseURL: appConfig.apiUrl,
  headers: {
    "Content-Type": "application/json",
  },
});

export const auth = {
  post: async (url, data) => {
    try {
      const resp = await instance.post(appConfig.apiUrl + url, data);
      return resp.data;
    } catch (error) {
      useMessage.setMessageError("Your email or password is incorrect");
    }
  },
};
