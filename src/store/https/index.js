import { appConfig } from "../../configs/env";
import axios from "axios";
import { useMessage } from "../../global/useMessage";

const instance = axios.create({
  baseURL: appConfig.apiUrl,
  headers: {
    "Content-Type": "application/json",
    Authorization: `Bearer ${localStorage.getItem("token")}`,
  },
});

export const https = {
  get: async (url) => {
    try {
      const resp = await instance.get(appConfig.apiUrl + url);
      return resp.data;
    } catch (error) {
      useMessage.setMessageError("Ops, something wrong!");
    }
  },
  post: async (url, data) => {
    try {
      const resp = await instance.post(appConfig.apiUrl + url, data);
      return resp.data;
    } catch (error) {
      useMessage.setMessageError("Ops, something wrong!");
    }
  },
  put: async (url, data) => {
    try {
      const resp = await instance.put(appConfig.apiUrl + url, data);
      return resp.data;
    } catch (error) {
      useMessage.setMessageError("Ops, something wrong!");
    }
  },
  patch: async (url, data) => {
    try {
      const resp = await instance.patch(appConfig.apiUrl + url, data);
      return resp.data;
    } catch (error) {
      useMessage.setMessageError("Ops, something wrong!");
    }
  },
  del: async (url) => {
    try {
      const resp = await instance.delete(appConfig.apiUrl + url);
      return resp.data;
    } catch (error) {
      useMessage.setMessageError("Ops, something wrong!");
    }
  },
  upload: async (url, data) => {
    try {
      let req = await instance.post(appConfig.apiUrl + url, data, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      });
      return req.data;
    } catch (error) {
      useMessage.setMessageError("Ops, something wrong!");
    }
  },
};
