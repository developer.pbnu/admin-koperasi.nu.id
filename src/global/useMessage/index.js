import { action, observable } from "mobx";
import toast from "react-hot-toast";

export const useMessage = observable({
  setMessageSuccess: action((message) => {
    toast.success(message);
  }),
  setMessageError: action((message) => {
    toast.error(message);
  }),
});
