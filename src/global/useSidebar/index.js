import { action, observable } from "mobx";

export const useSidebar = observable({
  status: true,
  open: action(() => {
    useSidebar.status = true;
  }),
  close: action(() => {
    useSidebar.status = false;
  }),
  ticket: 0,
  setTicket: action((values) => {
    useSidebar.ticket = values;
  }),
});
