import { action, observable } from "mobx";

export const useCounter = observable({
  count: 0,
  increment: action(() => {
    useCounter.count++;
  }),
  decrement: action(() => {
    useCounter.count--;
  }),
});
