import React, { useState } from "react";
import { Link } from "react-router-dom";
import { observer } from "mobx-react";
import { useGetAnggotaUnverify } from "../../../hooks/anggota";
import { MainLayout } from "../../../layouts/mainLayout";
import { Search, TableOne } from "../../../components";

export const AnggotaCalon = observer(() => {
  const { data, isLoading, isSuccess } = useGetAnggotaUnverify();
  const [search, setSeacrch] = useState("");
  const dataSearch = data?.data.filter((dt) => {
    return (
      dt?.name?.toLowerCase().includes(search.toLowerCase()) ||
      dt?.nik?.toString().includes(search.toString()) ||
      dt?.phone?.toString().includes(search.toString())
    );
  });

  const columns = [
    {
      title: "No",
      dataIndex: "no",
      render: (text, record, index) => {
        return index + 1;
      },
      width: 10,
    },
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Nik",
      dataIndex: "nik",
      key: "nik",
    },
    {
      title: "Phone",
      dataIndex: "phone_number",
      key: "phone_number",
    },
    {
      title: "Action",
      key: "action",
      width: 150,
      render: (text, record) => (
        <span>
          <Link to={`/anggota-unverified/${record.slug}`}>
            <div className="flex gap-2 p-1 pr-4 items-center rounded-full bg-red-500 hover:bg-red-600 text-white w-max uppercase text-xs">
              <span className="h-6 w-6 flex justify-center items-center bg-red-400 rounded-full">
                {record.name[0]}
              </span>
              <span>Detail</span>
            </div>
          </Link>
        </span>
      ),
    },
  ];

  return (
    <MainLayout
      breadcrumbs
      dataBreadcrumbs={[
        {
          title: "anggota unverified",
          to: "/anggota-unverified",
          end: true,
        },
      ]}
    >
      <div className="p-2 bg-white border border-gray-200 rounded-lg my-2">
        <div className="mb-2 flex items-center gap-2">
          <Search onChange={(e) => setSeacrch(e.target.value)} />
        </div>
        <TableOne
          rowKey={(record) => record.slug}
          dataSource={isSuccess && dataSearch}
          columns={columns}
          loading={isLoading}
        />
      </div>
    </MainLayout>
  );
});
