import { observer } from "mobx-react";
import moment from "moment";
import React, { useEffect } from "react";
import { FiCheck, FiX } from "react-icons/fi";
import { useNavigate, useParams } from "react-router-dom";
import { Loading } from "../../../../components/loading";
import { useGetOneAnggota, useVerify } from "../../../../hooks/anggota";
import { MainLayout } from "../../../../layouts/mainLayout";
import { Profile } from "./Module/Profile";

export const AnggotaUnverifiedShow = observer(() => {
  const { slug } = useParams();
  const navigate = useNavigate();
  const { data, isSuccess } = useGetOneAnggota(slug);
  const { mutate, isSuccess: isSuccessVerify, isLoading } = useVerify(slug);

  console.log(data);
  const verify = (e) => {
    if (e === "approve") {
      mutate({
        is_verified: true,
      });
    } else {
      mutate({
        is_verified: false,
      });
    }
  };

  useEffect(() => {
    if (isSuccessVerify) {
      navigate(-1);
    }
  }, [isSuccessVerify, navigate]);

  return (
    <MainLayout
      breadcrumbs
      dataBreadcrumbs={[
        {
          title: "anggota unverified",
          to: "/anggota-unverified",
        },
        {
          title: isSuccess ? data?.data?.name : "No Name",
          to: `/anggota-unverified/${slug}`,
          end: true,
        },
      ]}
    >
      {isLoading && <Loading />}
      {isSuccess && (
        <>
          <div className="bg-white rounded-lg">
            <div className="mx-auto my-5 p-2">
              <div className="md:flex no-wrap md:-mx-2 ">
                <div className="w-full md:w-3/12 md:mx-2">
                  <div className="bg-gray-50 p-3 border-t-4 border-gray-400 rounded-lg">
                    <div className="image overflow-hidden p-6">
                      <img
                        className="h-auto w-full mx-auto rounded-lg"
                        src={data?.data?.profile_photo}
                        alt="img"
                      />
                    </div>
                    <h1 className="text-gray-700 font-bold text-xl leading-8 my-2">
                      {data?.data.name}
                    </h1>
                    <h3 className="text-gray-600 font-lg text-semibold leading-6">
                      ID : {data?.data.member_id}
                    </h3>
                    <p className="text-sm text-gray-500 hover:text-gray-600 leading-6">
                      Secara resmi diterima sebagai anggota koperasi pada
                      tanggal {data?.data.registered}
                    </p>
                    <ul className="bg-gray-100 text-gray-600 hover:text-gray-700 hover:shadow py-2 px-3 mt-3 divide-y rounded shadow-sm">
                      <li className="flex items-center py-3">
                        <span>Status</span>
                        <span className="ml-auto">
                          <span className="bg-orange-500 py-1 px-2 rounded text-white text-sm">
                            Pedding
                          </span>
                        </span>
                      </li>
                      <li className="flex items-center py-3">
                        <span>Registered</span>
                        <span className="ml-auto">
                          {moment(data?.data.registered_at).fromNow()}
                        </span>
                      </li>
                    </ul>
                  </div>
                </div>
                <div className="w-full md:w-9/12 my-4 md:my-0 h-64">
                  <div className="bg-gray-50 p-3 shadow-sm rounded-lg">
                    <div className="flex items-center space-x-2 font-semibold text-gray-900 leading-8 mb-2">
                      <span clas="text-gray-500">
                        <svg
                          className="h-5"
                          xmlns="http://www.w3.org/2000/svg"
                          fill="none"
                          viewBox="0 0 24 24"
                          stroke="currentColor"
                        >
                          <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            strokeWidth={2}
                            d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z"
                          />
                        </svg>
                      </span>
                      <span className="tracking-wide">Profil</span>
                    </div>
                    <Profile data={data?.data} />
                  </div>
                  <div className="my-4" />
                  <div className="bg-gray-50 p-3 shadow-sm rounded-lg">
                    <div className="flex justify-end">
                      <div className="flex gap-2">
                        <div className="my-2">
                          <button
                            onClick={() => verify("approve")}
                            className="bg-teal-400 hover:bg-teal-700 font-bold text-white py-2 px-4 rounded-lg flex gap-2 items-center"
                          >
                            <FiCheck />
                            <span>Approve</span>
                          </button>
                        </div>
                        <div className="my-2">
                          <button
                            onClick={() => verify("reject")}
                            className="bg-orange-400 hover:bg-orange-700 font-bold text-white py-2 px-4 rounded-lg flex gap-2 items-center"
                          >
                            <FiX />
                            <span>Reject</span>
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </>
      )}
    </MainLayout>
  );
});
