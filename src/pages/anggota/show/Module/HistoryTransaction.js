import React from "react";
import { Steps } from "antd";

const { Step } = Steps;

export const HistoryTransaction = () => {
  const data = [
    {
      title: "11/03/22 - Pembayaran",
      description: "Pembayaran dari anggota",
    },
    {
      title: "12/03/22 - Pembayaran",
      description: "Pembayaran dari anggota",
    },
    {
      title: "13/03/22 - Pembayaran",
      description: "Pembayaran dari anggota",
    },
  ];
  return (
    <>
      <Steps progressDot direction="vertical">
        {data.map((item, index) => (
          <Step key={index} title={item.title} description={item.description} />
        ))}
      </Steps>
      <div className="flex justify-end mt-2 pt-2 border-t border-dashed">
        <button className="py-1 px-2 rounded bg-gray-200">
          Lihat lebih banyak
        </button>
      </div>
    </>
  );
};
