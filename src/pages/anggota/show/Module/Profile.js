import React from "react";

export const Profile = ({ data }) => {
  return (
    <>
      <div className="text-gray-700">
        <div className="grid md:grid-cols-2 text-sm">
          <div className="grid grid-cols-2">
            <div className="py-2 font-semibold">Nama</div>
            <div className="py-2">{data?.name}</div>
          </div>
          <div className="grid grid-cols-2">
            <div className="py-2 font-semibold">Nomor Induk Kependudukan</div>
            <div className="py-2">{data?.nik}</div>
          </div>
          <div className="grid grid-cols-2">
            <div className="py-2 font-semibold">Jenis Kelamin</div>
            <div className="py-2">{data?.gender}</div>
          </div>
          <div className="grid grid-cols-2">
            <div className="py-2 font-semibold">Phone</div>
            <div className="py-2">{data?.phone_number}</div>
          </div>
          <div className="grid grid-cols-2">
            <div className="py-2 font-semibold">Alamat</div>
            <div className="py-2">{data?.address}</div>
          </div>
          <div className="grid grid-cols-2">
            <div className="py-2 font-semibold">Kabupaten</div>
            <div className="py-2">{data?.city}</div>
          </div>
          <div className="grid grid-cols-2">
            <div className="py-2 font-semibold">Provinsi</div>
            <div className="py-2">{data?.province}</div>
          </div>
          <div className="grid grid-cols-2">
            <div className="py-2 font-semibold">Golongan Darah</div>
            <div className="py-2">{data?.blood_type}</div>
          </div>
        </div>
      </div>
    </>
  );
};
