import { Attention } from "./attention";
import { Auth } from "./auth";
import { Error404 } from "./errors/404";
import { ComingSoon } from "./errors/comingSoon";
import { Dashboard } from "./dashboard";
import { Anggota } from "./anggota";
import { Settings } from "./settings";
import { Invitation } from "./invitation";
import { AnggotaShow } from "./anggota/show";
import { AnggotaCalon } from "./anggota/calon";
import { AnggotaUnverifiedShow } from "./anggota/calon/show";
import { ManagemenAssets } from "./management_assets";
import { ManagemenAssetsAdminCabang } from "./management_assets/cabang";
import { ManagemenAssetsAdminCabangShow } from "./management_assets/cabang/show";
import { Ticket } from "./ticket";
import { TicketShow } from "./ticket/show";

export {
  Attention,
  Auth,
  Error404,
  ComingSoon,
  Dashboard,
  Anggota,
  Settings,
  Invitation,
  AnggotaShow,
  AnggotaCalon,
  AnggotaUnverifiedShow,
  Ticket,
  TicketShow,
  ManagemenAssets,
  ManagemenAssetsAdminCabang,
  ManagemenAssetsAdminCabangShow,
};
