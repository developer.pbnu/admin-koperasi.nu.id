/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from "react";
import { Form, Spin, Input } from "antd";
import { observer } from "mobx-react-lite";
import { useParams } from "react-router-dom";
import {
  useClosedMessageTicket,
  useGetMessageTicket,
  usePostMessageTicket,
} from "../../../hooks/tiket";
import { LoadingOutlined } from "@ant-design/icons";
import { useMessage } from "../../../global/useMessage";
import moment from "moment";
import { MainLayout } from "../../../layouts/mainLayout";

export const TicketShow = observer(() => {
  const { slug } = useParams();
  const [form] = Form.useForm();
  const {
    data: datanya,
    isSuccess,
    refetch,
    isLoading,
  } = useGetMessageTicket({ slug });

  const { mutate: mutateClose, isSuccess: isSuccessClose } =
    useClosedMessageTicket({ slug });

  const { mutate, isSuccess: isSuccessnya } = usePostMessageTicket({ slug });
  const data = isSuccess ? datanya : [];
  const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />;

  useEffect(() => {
    if (isSuccessClose) {
      refetch();
    }
    if (isSuccessnya) {
      form.resetFields();
      refetch();
      useMessage.setMessageSuccess("message sent");
    }
  }, [form, isSuccessnya, isSuccessClose, refetch]);

  console.log(data);
  return (
    <MainLayout>
      {isLoading ? (
        <div className="flex justify-center items-center">
          <div>
            <div className="flex justify-center items-center">
              <Spin indicator={antIcon} />
            </div>
            <p className="text-center mt-4 text-gray-500 lowercase tracking-widest text-xs">
              loading...
            </p>
          </div>
        </div>
      ) : (
        <div className="grid grid-cols-1 md:grid-cols-2 gap-4">
          <div className="w-full rounded-lg  shadow-none md:shadow-lg relative">
            <div className="w-full p-4 bg-gray-200 rounded-t-lg text-gray-800">
              <div className="w-full">
                <div className="grid grid-cols-2 gap-2">
                  <span className="capitalize">Ticket ID</span>
                  <span className="capitalize"> {data?.meta?.ticket_id}</span>
                  <span className="capitalize">Subject</span>
                  <span className="capitalize"> {data?.meta?.subject}</span>
                </div>
              </div>
            </div>
            <div className="p-4 overflow-auto bg-white h-[700px] pb-20">
              {data?.data
                ?.slice(0)
                .reverse()
                .map((dt, index) => {
                  return (
                    <div
                      className={`${
                        dt?.position !== "customer"
                          ? "flex justify-end text-right"
                          : ""
                      } w-full my-2 text-xs md:text-sm`}
                      key={index}
                    >
                      <div
                        className={`${
                          dt?.position !== "customer"
                            ? "bg-green-100 rounded-br-none"
                            : "bg-red-100 rounded-bl-none"
                        } py-1 px-4 rounded-xl w-max`}
                      >
                        {dt?.position !== "customer" ? (
                          <div>
                            <span className="text-[10px]">me</span>
                          </div>
                        ) : (
                          <div>
                            <span className="text-[10px]">{dt?.name}</span>
                          </div>
                        )}
                        <p>{dt?.message}</p>
                        <div className="flex justify-end">
                          <span className="text-[10px] italic">
                            {moment(dt?.created_at).fromNow()}
                          </span>
                        </div>
                      </div>
                    </div>
                  );
                })}
            </div>
            {data?.meta?.closed_at === null ? (
              <div className="flex justify-center w-full">
                <div className="p-4 w-full mx-auto bg-gray-50 rounded-lg">
                  <Form
                    form={form}
                    onFinish={(e) => {
                      mutate({
                        message: e.message,
                        file: null,
                      });
                    }}
                  >
                    <div className="flex gap-2 items-center">
                      <Form.Item
                        name="message"
                        style={{
                          width: "100%",
                        }}
                        rules={[
                          {
                            required: true,
                            message: "Please input your message!",
                          },
                        ]}
                      >
                        <Input placeholder="Message" className="w-full h-10" />
                      </Form.Item>
                      <Form.Item>
                        <button
                          type="submit"
                          className="bg-gray-800 text-white px-4 py-2 rounded"
                        >
                          Send
                        </button>
                      </Form.Item>
                    </div>
                  </Form>
                </div>
              </div>
            ) : (
              <div className="flex justify-center w-full">
                <div className="py-4 w-full mx-auto bg-red-500 bg-op bg-opacity-70 text-white">
                  <p className="uppercase font-bold text-white text-center">
                    CLOSED
                  </p>
                  <p className="text-center">Ticket sudah selesai</p>
                </div>
              </div>
            )}
          </div>
          <div className="w-full rounded-lg bg-white  shadow-none md:shadow-lg relative">
            <div className="w-full p-4 bg-gray-200 rounded-t-lg text-gray-800">
              <div className="w-1/2">
                <span className="font-bold">Detail</span>
              </div>
            </div>
            <div className="p-4 overflow-auto bg-white pb-20">
              <div className="grid grid-cols-2 gap-2">
                <span className="capitalize">Ticket ID</span>
                <span className="capitalize"> {data?.meta?.ticket_id}</span>
                <span className="capitalize">Subject</span>
                <span className="capitalize"> {data?.meta?.subject}</span>
                <span className="capitalize">opened at</span>
                <span className="capitalize">
                  {" "}
                  {moment(data?.meta?.opened_at).format("LLL")}
                </span>
                <span className="capitalize">priority</span>
                <span className="capitalize"> {data?.meta?.priority}</span>
                <span className="capitalize">slug</span>
                <span className="capitalize"> {data?.meta?.slug}</span>
                <span className="capitalize">subject</span>
                <span className="capitalize"> {data?.meta?.subject}</span>
                <span className="capitalize">support_ticket_category_id</span>
                <span className="capitalize">
                  {data?.meta?.support_ticket_category_id}
                </span>
                <span className="capitalize">Total Message</span>
                <span className="capitalize">{data?.data.length} Message</span>
              </div>
              {data?.meta?.closed_at === null ? (
                <div className="mt-10">
                  <button
                    onClick={() => mutateClose()}
                    className="py-2 text-white px-4 bg-red-800 rounded"
                  >
                    Close Ticket
                  </button>
                </div>
              ) : null}
            </div>
          </div>
        </div>
      )}
    </MainLayout>
  );
});
