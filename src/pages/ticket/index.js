import React, { useEffect, useState } from "react";
import { observer } from "mobx-react";
import { MainLayout } from "../../layouts/mainLayout";
import { useGetTicket, useTakeTicket } from "../../hooks/tiket";
import { TableTwo } from "../../components";
import { Button, Select, Table } from "antd";
import Search from "antd/lib/transfer/search";
import { useMessage } from "../../global/useMessage";
import { useNavigate } from "react-router-dom";

export const Ticket = observer(() => {
  const [status, setStatus] = React.useState("waiting");
  const [search, setSeacrch] = useState("");
  const navigate = useNavigate();
  const { data, isSuccess, isLoading, refetch } = useGetTicket({
    status,
  });

  const dataSearch =
    isSuccess &&
    data?.data.filter((dt) => {
      return (
        dt?.category?.toLowerCase().includes(search.toLowerCase()) ||
        dt?.customer?.toString().includes(search.toString()) ||
        dt?.subject?.toString().includes(search.toString())
      );
    });

  const { mutate, isSuccess: isSuccessMutate } = useTakeTicket();
  const applyTake = (slug) => {
    mutate(slug);
  };

  useEffect(() => {
    refetch();
    isSuccessMutate && useMessage.setMessageSuccess("Take ticket success");
  }, [isSuccessMutate, refetch, status]);

  const OpenTicket = (e) => {
    navigate(`/ticket/${e}`);
  };

  const columns = [
    {
      title: "No",
      dataIndex: "no",
      render: (text, record, index) => {
        return index + 1;
      },
      width: 10,
    },
    {
      title: "Ticket Id",
      dataIndex: "ticket_id",
      key: "ticket_id",
    },
    {
      title: "Support",
      dataIndex: "category",
      key: "category",
    },
    {
      title: "Member",
      dataIndex: "customer",
      key: "customer",
    },
    {
      title: "Subject",
      dataIndex: "subject",
      key: "subject",
    },
    {
      title: "Opened At",
      dataIndex: "opened_at",
      render: (text, record) => {
        return new Date(record.opened_at).toLocaleString();
      },
    },
    Table.EXPAND_COLUMN,
    {
      title: "Action",
      key: "action",
      render: (text, record) => (
        <>
          {status === "waiting" && (
            <span>
              <Button
                type="primary"
                onClick={() => {
                  applyTake(record.slug);
                }}
              >
                Take
              </Button>
            </span>
          )}
          {status === "open" && (
            <span>
              <Button onClick={() => OpenTicket(record?.slug)} type="primary">
                Open Ticket
              </Button>
            </span>
          )}
        </>
      ),
    },
  ];
  return (
    <MainLayout
      breadcrumbs
      dataBreadcrumbs={[
        {
          title: "ticket",
          to: "/ticket",
          end: true,
        },
      ]}
    >
      <div className="bg-white p-2 rounded-lg border border-gray-200">
        <div className="flex w-1/2 gap-4 mb-2">
          <Select
            className="w-2/6"
            defaultValue="waiting"
            onChange={(value) => {
              setStatus(value);
            }}
          >
            <Select.Option value="waiting">Waiting</Select.Option>
            <Select.Option value="open">Open</Select.Option>
            <Select.Option value="closed">Closed</Select.Option>
          </Select>
          <Search onChange={(e) => setSeacrch(e.target.value)} />
        </div>
        <TableTwo
          rowKey={(record) => record.slug}
          dataSource={isSuccess && dataSearch}
          columns={columns}
          loading={isLoading}
          total={500}
          expandedRowRender={(record) => (
            <div>
              <p>Message</p>
              <p>{record?.message}</p>
              <p className="lowercase text-xs italic text-blue-500">
                CLik open ticket to see more
              </p>
            </div>
          )}
        />
      </div>
    </MainLayout>
  );
});
