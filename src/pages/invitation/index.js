import React, { useEffect } from "react";
import {
  useAddInvitation,
  useDelInvitation,
  useGetInvitation,
} from "../../hooks/invitation";
import { MainLayout } from "../../layouts/mainLayout";
import { TableOne } from "../../components";

export const Invitation = () => {
  const [nik, setNik] = React.useState("");
  const { data, isLoading, isSuccess, refetch } = useGetInvitation();
  const { mutate: mutateAdd, isSuccess: isSuccessAdd } = useAddInvitation();
  const { mutate: mutateDel, isSuccess: isSuccessDel } = useDelInvitation();

  const validation = () => {
    const pattern = /^[0-9]{16}$/;
    if (!pattern.test(nik)) {
      return "NIK harus berupa angka 16 digit";
    }
    return "";
  };

  useEffect(() => {
    refetch();
    setNik("");
  }, [isSuccessAdd, refetch, isSuccessDel]);

  const handleSubmit = (e) => {
    e.preventDefault();
    mutateAdd({
      nik: parseInt(nik),
    });
  };

  const handleDelete = (slug) => {
    mutateDel(slug);
  };

  const columns = [
    {
      title: "No",
      dataIndex: "no",
      render: (text, record, index) => {
        return index + 1;
      },
      width: 10,
    },
    {
      title: "NIK",
      dataIndex: "nik",
      key: "nik",
    },
    {
      title: "Action",
      key: "action",
      width: 10,
      render: (text, record) => (
        <button onClick={() => handleDelete(record?.slug)}>Delete</button>
      ),
    },
  ];
  return (
    <MainLayout
      breadcrumbs
      dataBreadcrumbs={[
        {
          title: "Invitation",
          to: "/invitation",
          end: true,
        },
      ]}
    >
      <div className="grid grid-cols-1 md:grid-cols-2 gap-4">
        <div className="bg-white p-2 rounded-lg border border-gray-200">
          <TableOne
            rowKey="nik"
            dataSource={isSuccess && data?.data}
            columns={columns}
            loading={isLoading}
          />
        </div>
        <div className="bg-white p-2 rounded-lg border border-gray-200">
          <div className="mb-2">
            <p>Form Add Invitation</p>
          </div>
          <p className="text-xs mb-2">
            Masukkan Nomor Induk Kependudukan yang akan diundang
          </p>
          <div className="w-full bg-gray-100 p-2 border-gray-200 rounded-lg">
            <input
              onChange={(e) => setNik(e.target.value)}
              value={nik}
              className="w-1/2 py-2 px-4 rounded border border-gray-200 my-1 focus:ring-1 focus:ring-gray-300  focus:outline-none"
              placeholder="NIK"
              type="text"
            />
            {validation() === "" && (
              <button
                onClick={handleSubmit}
                type="button"
                className="py-2 px-4 text-white rounded-lg ml-4 bg-cyan-500 shadow-lg shadow-cyan-500/50"
              >
                Submit
              </button>
            )}
          </div>
          {validation() && (
            <span>
              <p className="text-red-500 text-xs italic mt-2">{validation()}</p>
            </span>
          )}
        </div>
      </div>
    </MainLayout>
  );
};
