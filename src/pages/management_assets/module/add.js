import React, { useEffect, useState } from "react";
import {
  Form,
  Input,
  Radio,
  DatePicker,
  InputNumber,
  Modal,
  Button,
  Select,
} from "antd";
import { useAddAssets } from "../../../hooks/management_assets";

const { Option } = Select;


export const formatterNumber = (val) => {
  if (!val) return 0;
  return `${val}`.replace(/\B(?=(\d{3})+(?!\d))/g, ".").replace(/\.(?=\d{0,2}$)/g, ",");
}

export const parserNumber = (val) => {
  if (!val) return 0;
  return Number.parseFloat(val.replace(/\$\s?|(\.*)/g, "").replace(/(\\,{1})/g, ".")).toFixed(2)
}


export const Add = ({ visible, onCancel, success }) => {
  const [form] = Form.useForm();
  const [typeAssets, setTypeAssets] = useState("cash");
  const { mutate: mutateAdd, isSuccess } = useAddAssets();

  useEffect(() => {
    if (isSuccess) {
      success();
    }
  }, [isSuccess, success]);

  const type = (type) => {
    switch (type) {
      case "cash":
        return "Cash";
      case "credit":
        return "Credit";
      case "grant":
        return "Hibah";
      default:
        return "Cash";
    }
  };

  const onFinish = async () => {
    const res = await form.validateFields();
    const data = {
      name: res.name,
      code: res.code,
      quantity: parseInt(res.quantity),
      source: typeAssets,
      price: parseInt(res.price),
      bought_date: res.bought_date,
      condition_range: res.condition_range,
      documents: ["file-nya_gan.jpg"],
      detail: res.detail,
      credit:
        typeAssets === "credit"
          ? {
            instalment: parseInt(res.instalment),
            period: parseInt(res.period),
            interest: parseInt(res.interest),
            detail: res?.detailcredit,
          }
          : null,
    }
    try {
      await mutateAdd(data);
      onCancel();
    }
    catch (err) {
      console.log(err);
    }
    form.resetFields();
    onCancel();
  };

  const onReset = () => {
    form.resetFields();
  };

  return (
    <>
      <Modal
        title={`Assets ${type(typeAssets)}`}
        visible={visible}
        onCancel={onCancel}
        width={1000}
        footer={
          <div className="flex justify-between">
            <Button onClick={onReset}>Reset</Button>
            <div>
              <Button onClick={onCancel}>Cancel</Button>
              <Button onClick={onFinish} type="primary" htmlType="submit">
                Submit
              </Button>
            </div>
          </div>
        }
      >
        <Form
          form={form}
          labelCol={{
            span: 8,
          }}
          onFinish={onFinish}
          wrapperCol={{
            span: 14,
          }}
          layout="horizontal"
        >
          <Form.Item label="Type" name="type">
            <Radio.Group>
              <Radio.Button onChange={() => setTypeAssets('cash')} value="cash">Cash</Radio.Button>
              <Radio.Button onChange={() => setTypeAssets('grant')} value="grant">Hibah</Radio.Button>
              <Radio.Button onChange={() => setTypeAssets('credit')} value="credit">Credit</Radio.Button>
            </Radio.Group>
          </Form.Item>
          <Form.Item label="Name" name="name"
            rules={[
              {
                required: true,
                message: <span className="text-xs">Required</span>,
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item label="Code" name="code"
            rules={[
              {
                required: true,
                message: <span className="text-xs">Required</span>,
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item label="Quantity" name="quantity"
            rules={[
              {
                required: true,
                message: <span className="text-xs">Required</span>,
              },
              {
                pattern: /^(?:\d*)$/,
                message: "Just number value",
              },
              {
                min: 1,
                max: 7,
                message: <span className="text-xs">Minimal 1 and Maksimal 1.000.000</span>,
              },
            ]}
          >
            <Input style={{
              width: "100%",
            }} />
          </Form.Item>
          <Form.Item label="Price" name="price"
            rules={[
              {
                required: true,
                message: <span className="text-xs">Required</span>,
              },
            ]}>
            <InputNumber
              prefix="Rp"
              style={{
                width: "100%",
              }}
              formatter={(value) => formatterNumber(value)}
              parser={(value) => parserNumber(value)}
            />

          </Form.Item>
          <Form.Item label={`${typeAssets === "credit" ? "Mulai Credit" : "Tanggal"}`} name="bought_date"
            rules={[
              {
                required: true,
                message: <span className="text-xs">Required</span>,
              },
            ]}
          >
            <DatePicker style={{
              width: "100%",
            }} />
          </Form.Item>
          <Form.Item label="Kondisi asets (10 - 100 point)" name="condition_range"
            rules={[
              {
                required: true,
                message: <span className="text-xs">Required</span>,
              },
            ]}
          >
            <Select defaultValue={10} style={{ width: "100%" }}>
              <Option value={10}>10</Option>
              <Option value={20}>20</Option>
              <Option value={30}>30</Option>
              <Option value={40}>40</Option>
              <Option value={50}>50</Option>
              <Option value={60}>60</Option>
              <Option value={70}>70</Option>
              <Option value={80}>80</Option>
              <Option value={90}>90</Option>
              <Option value={100}>100</Option>
            </Select>
          </Form.Item>
          <Form.Item label="Detail" name="detail">
            <Input />
          </Form.Item>

          {typeAssets === "credit" && (
            <>
              <Form.Item
                label="Cicilan Bulanan" name="instalment"
                rules={[
                  {
                    required: true,
                    message: <span className="text-xs">Required</span>,
                  },
                ]}>
                <InputNumber
                  prefix="Rp"
                  style={{
                    width: "100%",
                  }}
                  formatter={(value) => formatterNumber(value)}
                  parser={(value) => parserNumber(value)}
                />

              </Form.Item>
              <Form.Item label="Total Bulan" name="period" rules={[
                {
                  required: true,
                  message: <span className="text-xs">Required</span>,
                },
                {
                  pattern: /^[0-9]*$/,
                  message: <span className="text-xs">Must be number </span>,
                },
              ]}>
                <Input />
              </Form.Item>
              <Form.Item label="Bunga" name="interest"
                rules={[
                  {
                    required: true,
                    message: <span className="text-xs">Required</span>,
                  },
                ]}
              >
                <InputNumber
                  prefix="Rp"
                  style={{
                    width: "100%",
                  }}
                  formatter={(value) => formatterNumber(value)}
                  parser={(value) => parserNumber(value)}
                />

              </Form.Item>
              <Form.Item label="Detail Credit" name="detailcredit">
                <Input />
              </Form.Item>
            </>
          )}
        </Form>
      </Modal>
    </>
  );
};
