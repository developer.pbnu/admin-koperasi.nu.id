import React, { useState } from "react";
import { observer } from "mobx-react";
import { MainLayout } from "../../../../layouts/mainLayout";
import { useGetAllAssets } from "../../../../hooks/management_assets";
import { useParams } from "react-router-dom";
import { Search, TableTwo } from "../../../../components";
import { Table } from "antd";

export const ManagemenAssetsAdminCabangShow = observer(() => {
  const { slug } = useParams();
  const { data, isSuccess, isLoading } = useGetAllAssets(slug);

  const [search, setSeacrch] = useState("");
  const dataSearch = data?.data?.filter((dt) => {
    return (
      dt?.name?.toLowerCase().includes(search.toLowerCase()) ||
      dt?.nik?.toString().includes(search.toString()) ||
      dt?.phone?.toString().includes(search.toString())
    );
  });

  const columns = [
    {
      title: "No",
      dataIndex: "no",
      render: (text, record, index) => {
        return index + 1;
      },
      width: 10,
    },
    Table.EXPAND_COLUMN,
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Code Asset",
      dataIndex: "code",
      key: "code",
    },
    {
      title: "Kondisi",
      dataIndex: "condition_range",
      render: (text, record) => {
        if (record.condition_range >= 80) {
          return (
            <span className="py-1 rounded-full bg-green-300 text-green-600 px-4 text-xs">
              Sangat Baik
            </span>
          );
        } else if (
          record.condition_range >= 60 &&
          record.condition_range < 80
        ) {
          return (
            <span className="py-1 rounded-full bg-yellow-300 text-yellow-600 px-4 text-xs">
              Baik
            </span>
          );
        } else if (
          record.condition_range >= 40 &&
          record.condition_range < 60
        ) {
          return (
            <span className="py-1 rounded-full bg-orange-300 text-orange-600 px-4 text-xs">
              Cukup
            </span>
          );
        } else {
          return (
            <span className="py-1 rounded-full bg-red-300 text-red-600 px-4 text-xs">
              Kurang
            </span>
          );
        }
      },
    },
    {
      title: "Harga Beli",
      dataIndex: "price",
      key: "price",
      render: (text, record) => {
        return record.price
          .toString()
          .replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
      },
    },
  ];

  const numberFormat = (value) => {
    return value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
  };

  return (
    <MainLayout
      breadcrumbs
      dataBreadcrumbs={[
        {
          title: "Management Assets Cabang",
          to: "/management-assets/cabang",
        },
        {
          title: data?.meta?.name,
          to: `/management-assets/cabang/${slug}`,
          end: true,
        },
      ]}
    >
      <div className="bg-white p-2 rounded-lg border border-gray-200">
        <div className="mb-2 flex items-center gap-2">
          <Search onChange={(e) => setSeacrch(e.target.value)} />
        </div>
        <TableTwo
          rowKey={(record) => record.slug}
          dataSource={isSuccess && dataSearch}
          columns={columns}
          loading={isLoading}
          total={500}
          expandedRowRender={(record) => (
            <div className="grid grid-cols-2 gap-2">
              <div>
                <div className="flex items-center">
                  <p className="w-1/2 capitalize font-bold">Source</p>
                  <p className="pl-1 uppercase">{record?.source}</p>
                </div>
                {record?.source === "credit" && (
                  <div className="grid grid-cols-2 grid-flow-row-dense gap-2">
                    <p className="capitalize font-bold">instalment</p>
                    <p>{numberFormat(record?.credit?.instalment)}</p>
                    <p className="capitalize font-bold">interest</p>
                    <p>{numberFormat(record?.credit?.interest)}</p>
                    <p className="capitalize font-bold">period</p>
                    <p>{record?.credit?.period}</p>
                    <p className="capitalize font-bold">total</p>
                    <p>{numberFormat(record?.credit?.total)}</p>
                    <p className="capitalize font-bold">detail</p>
                    <p>{record?.credit?.detail}</p>
                  </div>
                )}
              </div>
              <div>
                <div className="flex items-center">
                  <p className="w-1/2 capitalize font-bold">documents</p>
                  <div>
                    {record?.documents === null
                      ? `-`
                      : record?.documents.map((dt, index) => {
                        return (
                          <div key={index}>
                            <a rel="noreferrer" target="_blank" href={dt}>
                              Doc {index + 1}
                            </a>
                          </div>
                        );
                      })}
                  </div>
                </div>
                <div className="flex items-center">
                  <p className="w-1/2 capitalize font-bold">detail</p>
                  <p>{record?.detail || `-`}</p>
                </div>
              </div>
            </div>
          )}
        />
      </div>
    </MainLayout>
  );
});
