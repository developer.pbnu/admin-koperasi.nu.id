import React, { useState } from "react";
import { observer } from "mobx-react";
import { MainLayout } from "../../../layouts/mainLayout";
import { useGetAdminManagementAssets } from "../../../hooks/management_assets";
import { Link } from "react-router-dom";
import { Search, TableOne } from "../../../components";

export const ManagemenAssetsAdminCabang = observer(() => {
  const { data, isSuccess, isLoading } = useGetAdminManagementAssets();
  const [search, setSeacrch] = useState("");
  const dataSearch = data?.data.filter((dt) => {
    return (
      dt?.name?.toLowerCase().includes(search.toLowerCase()) ||
      dt?.nik?.toString().includes(search.toString()) ||
      dt?.phone?.toString().includes(search.toString())
    );
  });

  const columns = [
    {
      title: "No",
      dataIndex: "no",
      render: (text, record, index) => {
        return index + 1;
      },
      width: 10,
    },
    {
      title: "Name / Cabang",
      dataIndex: "name",
      render: (text, record) => {
        return (
          <Link to={`/management-assets/cabang/${record?.slug}`}>{text}</Link>
        );
      },
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
    },
    {
      title: "ID",
      dataIndex: "slug",
      key: "slug",
    },
    {
      title: "Phone Number",
      dataIndex: "phone_number",
      key: "phone_number",
    },
    {
      title: "Total Assets",
      dataIndex: "assets_count",
      key: "assets_count",
    },
  ];

  return (
    <MainLayout
      breadcrumbs
      dataBreadcrumbs={[
        {
          title: "Management Assets Cabang",
          to: "/management-assets/cabang",
          end: true,
        },
      ]}
    >
      <div className="bg-white p-2 rounded-lg border border-gray-200">
        <div className="mb-2 flex items-center gap-2">
          <Search onChange={(e) => setSeacrch(e.target.value)} />
        </div>
        <TableOne
          rowKey={(record) => record.slug}
          dataSource={isSuccess && dataSearch}
          columns={columns}
          loading={isLoading}
          total={500}
        />
      </div>
    </MainLayout>
  );
});
