import React from "react";
import { observer } from "mobx-react";
import { useNavigate } from "react-router-dom";
import { authenticationRepository } from "../../hooks/authorization";
import { useMessage } from "../../global/useMessage";
import { ReactComponent as SvgBottom } from "../../assets/svg/connected-dot-xl.svg";
import { ReactComponent as SvgTopRight } from "../../assets/svg/connected-dot-sm-3.svg";
import { ReactComponent as SvgTopLeft } from "../../assets/svg/connected-dot-sm-1.svg";
import { useMediaQuery } from "react-responsive";
import { Button, Form, Input } from "antd";

export const Auth = observer(() => {
  let navigate = useNavigate();
  const isDesktopOrLaptop = useMediaQuery({
    query: "(min-width: 1224px)",
  });

  const parseToken = (token) => {
    const base64Url = token.split(".")[1];
    const base64 = base64Url.replace("-", "+").replace("_", "/");
    const userdata = JSON.parse(window.atob(base64));
    return userdata;
  };

  const onFinish = async (values) => {
    try {
      const res = await authenticationRepository.api.login({
        email: values.email,
        password: values.password,
      });
      if (res.message === "success") {
        const t = parseToken(res.data.access_token);
        if (t.role !== "super-admin") {
          localStorage.clear();
          useMessage?.setMessageError(
            "You are not authorized to access this page"
          );
          navigate("/");
        } else {
          localStorage.setItem("token", res.data.access_token);
          navigate("/attention", { replace: true });
          window.location.reload();
        }
      } else {
        useMessage?.setMessageError("Invalid email or password");
      }
    } catch (e) { }
  };

  const onFinishFailed = (errorInfo) => {
    useMessage?.setMessageError("Login gagal, chek form anda");
  };
  return (
    <div>
      <div className="min-h-full flex items-center justify-center py-12 px-4 sm:px-6 lg:px-8">
        {isDesktopOrLaptop && (
          <>
            <SvgBottom className="w-2/12 h-auto absolute bottom-0 rotate-180 z-0" />
            <SvgTopRight className="w-16 h-auto absolute top-2 right-2 z-0" />
            <SvgTopLeft className="w-16 h-auto absolute top-2 left-5 z-0" />
          </>
        )}
        <div className="max-w-md w-full space-y-8 mt-40 md:mt-64">
          <div>
            <img
              className="mx-auto h-12 w-auto"
              src="https://tailwindui.com/img/logos/workflow-mark-indigo-600.svg"
              alt="Workflow"
            />
            <h2 className="mt-6 text-center text-3xl font-extrabold text-gray-700">
              Sign in to your account
            </h2>
            <p className="mt-2 text-center text-sm text-gray-600">
              Or
              <a
                href="https://koperasi.nu.id"
                className="font-medium text-primary-300 hover:text-primary-200"
              >
                {" "}
                back to home
              </a>
            </p>
          </div>
          <Form
            name="login"
            layout="vertical"
            labelCol={{ span: 8 }}
            wrapperCol={{ span: 24 }}
            initialValues={{ remember: true }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
          >
            <Form.Item
              name="email"
              rules={[
                {
                  required: true,
                  message: (
                    <span className="text-xs mb-6">
                      Please input your email!
                    </span>
                  ),
                },
                {
                  pattern: /^[^\s@]+@[^\s@]+\.[^\s@]+$/,
                  message: (
                    <span className="text-xs mb-6">
                      The input is not valid email!
                    </span>
                  ),
                },
              ]}
            >
              <Input
                style={{
                  padding: "10px",
                  borderColor: "rgba(0,0,0,.25)",
                  border: "1px solid #43b82e",
                  borderRadius: "30px",
                  paddingLeft: "20px",
                }}
                placeholder="Your email"
              />
            </Form.Item>

            <Form.Item
              name="password"
              rules={[
                {
                  required: true,
                  message: (
                    <span className="text-xs mb-6">
                      Please input your password!
                    </span>
                  ),
                },
              ]}
            >
              <Input.Password
                style={{
                  padding: "10px",
                  borderColor: "rgba(0,0,0,.25)",
                  border: "1px solid #43b82e",
                  borderRadius: "30px",
                  paddingLeft: "20px",
                }}
                placeholder="Your password"
              />
            </Form.Item>
            <Form.Item wrapperCol={{ span: 24 }}>
              <Button
                type="primary"
                htmlType="submit"
                style={{
                  width: "40%",
                  borderRadius: "30px",
                  padding: "10px",
                  height: "45px",
                  border: "0px",
                  backgroundColor: "#43b82e",
                }}
              >
                SUBMIT
              </Button>
              <a
                href="https://koperasi.nu.id/help"
                className="ml-4 text-gray-700"
              >
                Help center!
              </a>
            </Form.Item>
          </Form>
        </div>
      </div>
    </div>
  );
});
