import React from "react";
import { observer } from "mobx-react";
import { MainLayout } from "../../layouts/mainLayout";
import { Tabs } from "antd";

const { TabPane } = Tabs;
export const Settings = observer(() => {
  return (
    <MainLayout
      breadcrumbs
      dataBreadcrumbs={[
        {
          title: "Settings",
          to: "/settings",
          end: true,
        },
      ]}
    >
      <Tabs defaultActiveKey="1">
        <TabPane tab="Defaults" key="1">
          Content of Tab Pane 1
        </TabPane>
        <TabPane tab="Professions" key="2">
          Content of Tab Pane 2
        </TabPane>
        <TabPane tab="Hak akses" key="3">
          Content of Tab Pane 3
        </TabPane>
      </Tabs>
    </MainLayout>
  );
});
