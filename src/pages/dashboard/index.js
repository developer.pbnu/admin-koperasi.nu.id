import React from "react";
import { observer } from "mobx-react";
import { MainLayout } from "../../layouts/mainLayout";
import {
  Chart as ChartJS,
  RadialLinearScale,
  ArcElement,
  Tooltip,
  Legend,
} from "chart.js";
import { PolarArea } from "react-chartjs-2";
import { useGetProfessions } from "../../hooks/professions";
import Indonesia from "../../assets/svg/id.svg";

ChartJS.register(RadialLinearScale, ArcElement, Tooltip, Legend);

export const Dashboard = observer(() => {
  const { data: dataprof, isSuccess } = useGetProfessions();
  const profesions = [];
  if (isSuccess) {
    dataprof?.data.forEach((el) => {
      profesions.push(el.name);
    });
  }
  const data = {
    labels: profesions,
    datasets: [
      {
        label: "# of Votes",
        data: [12, 19, 3, 5, 2, 3],
        backgroundColor: [
          "rgba(255, 99, 132, 0.5)",
          "rgba(54, 162, 235, 0.5)",
          "rgba(255, 206, 86, 0.5)",
          "rgba(75, 192, 192, 0.5)",
          "rgba(153, 102, 255, 0.5)",
          "rgba(255, 159, 64, 0.5)",
        ],
        borderWidth: 1,
      },
    ],
  };
  const options = {
    responsive: true,
    plugins: {
      legend: {
        position: "bottom",
      },
    },
  };
  return (
    <MainLayout>
      <div className="w-full md:flex gap-2">
        <div className="w-full md:w-1/3 p-2 bg-gray-50 rounded-lg border border-gray-200">
          <div className="bg-white p-2 rounded-lg border border-gray-200">
            <p className="text-xs text-center my-2 uppercase">
              Data Profesi Anggota Koperasi Tahun 2022
            </p>
            <PolarArea data={data} options={options} />
          </div>
        </div>
        <div className="w-full md:w-2/3 p-2 bg-gray-50 rounded-lg border border-gray-200 mt-2 md:mt-0">
          <div className="bg-white p-2 rounded-lg border border-gray-200 h-full">
            <p className="text-xs text-center my-2 uppercase">Indonesia</p>
            <div className="flex justify-center items-center h-full">
              <img src={Indonesia} alt="indonesia" className="w-full" />
            </div>
          </div>
        </div>
      </div>
    </MainLayout>
  );
});
