import React, { useEffect, useRef } from "react";
import { useNavigate } from "react-router-dom";
import { Button, Select } from "antd";
import IdleTimer from "react-idle-timer";

const { Option } = Select;

export const Attention = () => {
  const navigate = useNavigate();
  const IdleTimerRef = useRef(null);

  const next = () => {
    navigate("/dashboard", { replace: true });
  };
  const [path, setPath] = React.useState("attention");
  function onChange(value) {
    setPath(value);
  }
  const jumpTo = () => {
    navigate(`/${path}`);
  };
  useEffect(() => {
    if (!localStorage.getItem("token")) {
      navigate("/404", { replace: true });
    }
  }, [navigate]);

  return (
    <div className="flex justify-center items-center min-h-screen w-full">
      <IdleTimer ref={IdleTimerRef} timeout={10000} onIdle={next} />
      <div>
        <p className="text-center mb-6 text-lg font-bold">
          Tanks for your action
        </p>
        <p className="text-center text-xs mb-6 capitalize">
          page will redirect to /dashboard if user is idle for more than 10
          seconds
        </p>
        <div className="flex justify-center items-center gap-2">
          <Select
            size="large"
            showSearch
            style={{ width: 200 }}
            placeholder="Select a menu"
            optionFilterProp="children"
            onChange={onChange}
            filterOption={(input, option) =>
              option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            }
          >
            <Option value="anggota">Anggota</Option>
            <Option value="anggota-unverified">Calon anggota</Option>
            <Option value="invitation">Invitation</Option>
            <Option value="settings">Settings</Option>
            <Option value="management-assets">Aset Pusat</Option>
            <Option value="management-assets/cabang">Aset cabang</Option>
            <Option value="user-management">User management</Option>
            <Option value="ticket">Ticket</Option>
          </Select>
          <div className="flex item-center gap-2">
            <Button type="primary" onClick={jumpTo} size="large">
              Jump To
            </Button>
            <Button onClick={next} type="primary" size="large">
              Dashboard
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
};
