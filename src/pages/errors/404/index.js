import React from 'react'

export const Error404 = () => {
  return (
    <div className='flex justify-center items-center min-h-screen w-full'>
      <p className='text-[40px] font-bold text-gray-400'>404</p>
    </div>
  )
}
