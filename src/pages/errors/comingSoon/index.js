import React from "react";
import { observer } from "mobx-react";
import { MainLayout } from "../../../layouts/mainLayout";

import Lottie from "lottie-react-web";
import comingsoon from "../../../assets/json/coming-soon.json";

export const ComingSoon = observer(() => {
  return (
    <MainLayout>
      <div>
        <Lottie
          width={200}
          height={200}
          options={{
            animationData: comingsoon,
          }}
        />
        <p className="text-center capitalize">Dalam proses pengembangan developer</p>
      </div>
    </MainLayout>
  );
});
