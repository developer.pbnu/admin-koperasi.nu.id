export class TokenUtil {
  static setAccessToken(accessToken) {
    localStorage.setItem("token", accessToken);
  }

  static setRefreshToken(refreshToken) {
    localStorage.setItem("refresh_token", refreshToken);
  }
  static setUser(user) {
    localStorage.setItem("user", user);
  }
  static setSlug(slug) {
    localStorage.setItem("slug", slug);
  }

  static clearAccessToken() {
    localStorage.removeItem("token");
  }

  static clearRefreshToken() {
    localStorage.removeItem("refresh_token");
  }
}
