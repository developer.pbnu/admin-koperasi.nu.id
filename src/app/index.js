import React from "react";
import { BrowserRouter } from "react-router-dom"; // BrowserRouter is a component that wraps the entire application
import { QueryClient, QueryClientProvider } from "react-query";
import { ReactQueryDevtools } from "react-query/devtools";
import { Router } from "../routers"; // <-- this is the router
import "../scss/index.css"; // <-- this is the css file
import { Toaster } from "react-hot-toast";
import 'antd/dist/antd.min.css';

export const App = () => {
  const queryClient = new QueryClient();
  return (
    <>
      <QueryClientProvider client={queryClient}>
        <BrowserRouter>
          <Toaster />
          <Router />
        </BrowserRouter>
        <ReactQueryDevtools initialIsOpen={false} />
      </QueryClientProvider>
    </>
  );
};
