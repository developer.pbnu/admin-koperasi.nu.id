import { useQuery } from "react-query";
import { https } from "../../store/https";

export const useGetProfessions = () => {
  const endpoint = "/professions?size=10&page=1";
  const key = "professions";
  const fn = () => {
    return https.get(endpoint);
  };
  const { data, isSuccess, refetch, isLoading } = useQuery(key, fn);
  return { data, isSuccess, refetch, isLoading };
};
