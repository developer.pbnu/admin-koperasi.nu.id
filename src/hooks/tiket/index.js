import { useMutation, useQuery } from "react-query";
import { https } from "../../store/https";

export const useGetTicketSupport = () => {
  const endpoint = "/support-tickets/categories";
  const key = "support-tickets-categories";
  const fn = () => {
    return https.get(endpoint);
  };
  const { data, isSuccess, refetch, isLoading } = useQuery(key, fn);
  return { data, isSuccess, refetch, isLoading };
};

export const useGetTicket = (props) => {
  const status = props?.status || "waiting";
  const endpoint = `/support-tickets?status=${status}`;
  const key = "tickets-all";
  const fn = () => {
    return https.get(endpoint);
  };
  const { data, isSuccess, refetch, isLoading } = useQuery(key, fn);
  return { data, isSuccess, refetch, isLoading };
};
export const useTakeTicket = () => {
  const fn = (e) => {
    return https.patch(`/support-tickets/${e}/taked`);
  };
  const { data, mutate, isSuccess } = useMutation((e) => fn(e));
  return { data, mutate, isSuccess };
};

export const useGetMessageTicket = (props) => {
  const endpoint = `/support-tickets/${props?.slug}/messages`;
  const key = "tickets-message";
  const fn = () => {
    return https.get(endpoint);
  };
  const { data, isSuccess, refetch, isLoading } = useQuery(key, fn, {
    refetchInterval: 3000,
  });
  return { data, isSuccess, refetch, isLoading };
};

export const usePostMessageTicket = (props) => {
  const endpoint = `/support-tickets/${props?.slug}/messages`;
  const fn = (e) => {
    return https.post(endpoint, e);
  };
  const { data, mutate, isSuccess } = useMutation((e) => fn(e));
  return { data, mutate, isSuccess };
};

export const useClosedMessageTicket = (props) => {
  const endpoint = `/support-tickets/${props?.slug}/closed`;
  const fn = (e) => {
    return https.patch(endpoint, e);
  };
  const { data, mutate, isSuccess } = useMutation((e) => fn(e));
  return { data, mutate, isSuccess };
};
