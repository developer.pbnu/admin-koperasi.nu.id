import { useMutation, useQuery } from "react-query";
import { https } from "../../store/https";

export const useGetAnggotaVerify = () => {
  const endpoint = "/members?page=1&size=10&status=verified";
  const key = "anggota-verify";
  const fn = () => {
    return https.get(endpoint);
  };
  const { data, isSuccess, refetch, isLoading } = useQuery(key, fn);
  return { data, isSuccess, refetch, isLoading };
};

export const useGetAnggotaUnverify = () => {
  const endpoint = "/members?page=1&size=10&status=unverified";
  const key = "anggota-unverify";
  const fn = () => {
    return https.get(endpoint);
  };
  const { data, isSuccess, refetch, isLoading } = useQuery(key, fn);
  return { data, isSuccess, refetch, isLoading };
};

export const useGetOneAnggota = (slug) => {
  const endpoint = `/members/${slug}`;
  const key = "anggota-one";
  const fn = () => {
    return https.get(endpoint);
  };
  const { data, isSuccess, refetch, isLoading } = useQuery(key, fn);
  return { data, isSuccess, refetch, isLoading };
};

export const useVerify = (slug) => {
  const endpoint = `/members/${slug}/verified`;
  const fn = (data) => {
    return https.put(endpoint, data);
  };
  const { data, mutate, isSuccess, isLoading } = useMutation((e) => fn(e));
  return { data, mutate, isSuccess, isLoading };
};
