import { useQuery } from "react-query";
import { https } from "../../store/https";

export const useGetProfile = () => {
  const endpoint = "/auth/profile";
  const key = "profile";
  const fn = () => {
    return https.get(endpoint);
  };
  const { data, isSuccess, refetch, isLoading } = useQuery(key, fn);
  return { data, isSuccess, refetch, isLoading };
};
