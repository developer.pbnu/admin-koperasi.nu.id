import { auth } from "../../store/auth";
import { TokenUtil } from "../../utils/token";

const url = {
  login: () => "/auth/signin",
};
const api = {
  async login({ email, password }) {
    const result = await auth.post(url.login(), {
      email,
      password,
    });

    const base64Url = result.data?.access_token.split(".")[1];
    const base64 = base64Url.replace("-", "+").replace("_", "/");
    const userdata = JSON.parse(window.atob(base64));

    TokenUtil.setUser(JSON.stringify(userdata));
    TokenUtil.setSlug(userdata?.sub);
    TokenUtil.setAccessToken(result?.data?.access_token);
    TokenUtil.setRefreshToken(result?.data?.refresh_token);

    return result;
  },

  async logOut() {
    TokenUtil.clearAccessToken();
    TokenUtil.clearRefreshToken();
    return "success";
  },
};

export const authenticationRepository = {
  url,
  api,
};
