import { useMutation, useQuery } from "react-query";
import { https } from "../../store/https";

export const useGetAdminManagementAssets = () => {
  const endpoint = "/admins?page=1&size=10";
  const key = "management-assets-admin";
  const fn = () => {
    return https.get(endpoint);
  };
  const { data, isSuccess, refetch, isLoading } = useQuery(key, fn);
  return { data, isSuccess, refetch, isLoading };
};
export const useGetAllAssets = () => {
  const slug = localStorage.getItem("slug");
  const endpoint = `/admins/${slug}/assets?page=1&size=10`;
  const key = "management-assets";
  const fn = () => {
    return https.get(endpoint);
  };
  const { data, isSuccess, refetch, isLoading } = useQuery(key, fn);
  return { data, isSuccess, refetch, isLoading };
};


export const useAddAssets = () => {
  const slug = localStorage.getItem("slug");
  const endpoint = `/admins/${slug}/assets`;
  const fn = (e) => {
    return https.post(endpoint, e);
  };
  const { data, mutate, isSuccess } = useMutation((e) => fn(e));
  return { data, mutate, isSuccess };
};