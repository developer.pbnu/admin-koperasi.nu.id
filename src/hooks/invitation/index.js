import { useMutation, useQuery } from "react-query";
import { https } from "../../store/https";

export const useGetInvitation = () => {
  const endpoint = "/invitations?size=10&page=1";
  const key = "invitation";
  const fn = () => {
    return https.get(endpoint);
  };
  const { data, isSuccess, refetch, isLoading } = useQuery(key, fn);
  return { data, isSuccess, refetch, isLoading };
};

export const useAddInvitation = () => {
  const endpoint = "/invitations";
  const fn = (e) => {
    return https.post(endpoint, e);
  };
  const { data, mutate, isSuccess } = useMutation((e) => fn(e));
  return { data, mutate, isSuccess };
};

export const useDelInvitation = () => {
  const endpoint = "/invitations";
  const fn = (e) => {
    return https.del(`${endpoint}/${e}`);
  };
  const { data, mutate, isSuccess } = useMutation((e) => fn(e));
  return { data, mutate, isSuccess };
};
