import { http } from "../../store/http"
const url = {
    uploadFile: () => {
        return "/files";
    },
};

const hooks = {};
const api = {
    uploadFile(data, type) {
        const formData = new FormData();
        formData.append("file", data);
        formData.append("type", type);
        return http.upload(url.uploadFile(), formData);
    },
};


export const fileRepository = {
    url,
    hooks,
    api,
};
