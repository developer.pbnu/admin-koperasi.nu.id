import { observer } from "mobx-react";
import React, { useEffect, useRef } from "react";
import { useSidebar } from "../../global/useSidebar";
import { Sidebar } from "./Module/Sidebar";
import { useMediaQuery } from "react-responsive";
import { FiToggleLeft, FiToggleRight } from "react-icons/fi";
import { useNavigate } from "react-router-dom";
import { useGetProfile } from "../../hooks/profile";
import { Breadcrumbs } from "../../components";
import IdleTimer from "react-idle-timer";

export const MainLayout = observer(
  ({ children, breadcrumbs = false, dataBreadcrumbs }) => {
    let navigate = useNavigate();
    const { data, isSuccess } = useGetProfile();
    const isDesktopOrLaptop = useMediaQuery({
      query: "(min-width: 1224px)",
    });

    const IdleTimerRef = useRef(null);
    const slug = isSuccess ? data?.data?.slug : "";
    localStorage.setItem("slug", slug);

    useEffect(() => {
      if (isDesktopOrLaptop) {
        useSidebar.open();
      } else {
        useSidebar.close();
      }
    }, [isDesktopOrLaptop, isSuccess]);

    const status = useSidebar.status;

    const handleLogout = () => {
      localStorage.clear();
      navigate("/");
    };

    if (
      localStorage.getItem("token") === "undefined" ||
      !localStorage.getItem("token")
    ) {
      useEffect(() => {
        navigate("/");
      }, [navigate]);
      return null;
    } else {
      return (
        <>
          <IdleTimer ref={IdleTimerRef} timeout={600000} onIdle={handleLogout} />
          <div className="flex min-h-screen">
            <div
              className={`${status ? "w-10/12 md:w-1/5 block" : "hidden"
                } shadow-lg bg-gray-900`}
            >
              <Sidebar />
            </div>
            <div
              className={`${status ? "w-2/12 md:w-full" : "flex-1"
                } bg-gray-100`}
            >
              <div className="z-50 mx-4 mt-4 flex justify-between">
                {status ? (
                  <button
                    onClick={() => useSidebar.close()}
                    className="bg-gray-200 p-2 rounded-lg"
                  >
                    <FiToggleRight className="text-lg" />
                  </button>
                ) : (
                  <button
                    onClick={() => useSidebar.open()}
                    className="bg-gray-200 p-2 rounded-lg flex gap-2 items-center text-xs uppercase"
                  >
                    <FiToggleLeft className="text-lg" />{" "}
                    <span className="text-gray-700">Menu</span>
                  </button>
                )}
                {isDesktopOrLaptop ? (
                  <>
                    <div className="flex items-center bg-gray-300 p-1 rounded-full pr-4">
                      <div className="p-1 h-6 w-6 rounded-full text-white bg-gray-600 flex items-center justify-center text-xs uppercase mr-2">
                        {isSuccess && data?.data?.name[0]}
                      </div>
                      <span className="text-gray-600 font-bold">
                        {isSuccess && data?.data?.name}
                      </span>
                    </div>
                  </>
                ) : (
                  !status && (
                    <div className="flex items-center bg-gray-300 p-1 rounded-full pr-4">
                      <div className="p-1 h-6 w-6 rounded-full text-white bg-gray-600 flex items-center justify-center text-xs uppercase mr-2">
                        {isSuccess && data?.data?.name[0]}
                      </div>
                      <span className="text-gray-600 font-bold">
                        {isSuccess && data?.data?.name}
                      </span>
                    </div>
                  )
                )}
              </div>
              <main
                className={`${status ? "hidden md:block" : "block"
                  } p-4 text-sm`}
              >
                {breadcrumbs && <Breadcrumbs data={dataBreadcrumbs} />}
                {children}
              </main>
            </div>
          </div>
        </>
      );
    }
  }
);
