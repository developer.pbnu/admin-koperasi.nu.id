import React from "react";
import { useNavigate } from "react-router-dom";
import { ButtonMenuSidebar } from "./ButtonMenuSidebar";
import {
  AiFillHome,
  AiOutlinePoweroff,
  AiOutlineUsergroupAdd,
  AiTwotoneGold,
  AiTwotoneSetting,
  AiOutlineProject,
  AiOutlineFileProtect,
  AiOutlineMessage,
  AiOutlineVerticalAlignBottom,
  AiOutlineVerticalAlignTop,
  AiOutlineSlack,
  AiOutlineUser,
  AiOutlineCloudDownload,
  AiTwotoneStar,
  AiOutlineNumber,
  AiOutlineEllipsis,
  AiOutlineUserSwitch,
  AiFillCreditCard,
  AiFillStar,
  AiOutlineRadiusBottomright,
  AiOutlineRadiusSetting,
} from "react-icons/ai";
import { DropdownMenu } from "./DropdownMenu";
import { ParticlesMode } from "../../../components";
import { useGetAnggotaUnverify } from "../../../hooks/anggota";
import { useGetTicket } from "../../../hooks/tiket";

export const Sidebar = () => {
  let navigate = useNavigate();
  const handleClick = (e) => {
    navigate(e);
  };

  const logout = () => {
    localStorage.clear();
    localStorage.removeItem("slug");
    localStorage.removeItem("token");
    localStorage.removeItem("refresh_token");
    navigate("/");
  };

  const pathname = window.location.pathname;

  const { data, isSuccess } = useGetAnggotaUnverify();

  const { data: dataWaiting, isSuccess: isSuccessWaiting } = useGetTicket({
    status: "waiting",
  });

  return (
    <div className="w-full pt-16 relative min-h-full">
      <ParticlesMode particles_color="#9ca3af" particles_line="#4b5563" />
      <div>
        <p className={`text-center mt-4 uppercase font-bold text-gray-400`}>
          koperasi.nu.id
        </p>
      </div>
      <div className="px-4 pt-6">
        <ButtonMenuSidebar
          title="Dashboard"
          icon={<AiFillHome className="h-5 w-auto" />}
          status={pathname === "/dashboard" ? true : false}
          onClick={() => handleClick("/dashboard")}
        />
        <DropdownMenu
          title="Anggota"
          icon={<AiTwotoneGold className="h-5 w-auto" />}
        >
          <ButtonMenuSidebar
            title="Anggota"
            icon={<AiOutlineUser className="h-4 w-auto" />}
            status={pathname === "/anggota" ? true : false}
            onClick={() => handleClick("/anggota")}
          />
          <ButtonMenuSidebar
            title="Calon Anggota"
            icon={<AiOutlineUsergroupAdd className="h-5 w-auto" />}
            status={pathname === "/anggota-unverified" ? true : false}
            onClick={() => handleClick("/anggota-unverified")}
            tagRight={isSuccess && data?.data.length}
          />
          <ButtonMenuSidebar
            title="Invitation"
            icon={<AiFillStar className="h-5 w-auto" />}
            status={pathname === "/invitation" ? true : false}
            onClick={() => handleClick("/invitation")}
          />
        </DropdownMenu>

        <DropdownMenu
          title="Simpanan"
          icon={<AiOutlineSlack className="h-5 w-auto" />}
        >
          <ButtonMenuSidebar
            title="Simpanan Wajib"
            icon={<AiOutlineVerticalAlignBottom className="h-5 w-auto" />}
            status={pathname === "/simpanan-wajib" ? true : false}
            onClick={() => handleClick("/coming-soon")}
          />
          <ButtonMenuSidebar
            title="Simpanan Pokok"
            icon={<AiOutlineVerticalAlignTop className="h-5 w-auto" />}
            status={pathname === "/simpanan-pokok" ? true : false}
            onClick={() => handleClick("/coming-soon")}
          />
        </DropdownMenu>

        <DropdownMenu
          title="Tabungan"
          icon={<AiOutlineCloudDownload className="h-5 w-auto" />}
        >
          <ButtonMenuSidebar
            title="Tab. Anggota"
            icon={<AiTwotoneStar className="h-4 w-auto" />}
            status={pathname === "/tab-anggota" ? true : false}
            onClick={() => handleClick("/coming-soon")}
          />
          <ButtonMenuSidebar
            title="Tab. Haji & Umroh"
            icon={<AiTwotoneStar className="h-4 w-auto" />}
            status={pathname === "/tab-haji-umroh" ? true : false}
            onClick={() => handleClick("/coming-soon")}
          />
          <ButtonMenuSidebar
            title="Tab. Idul Fitri & Idul Adha"
            icon={<AiTwotoneStar className="h-4 w-auto" />}
            status={pathname === "/tab-fitri-adha" ? true : false}
            onClick={() => handleClick("/coming-soon")}
          />
        </DropdownMenu>

        <DropdownMenu
          title="Kas & Bank"
          icon={<AiOutlineNumber className="h-5 w-auto" />}
        >
          <ButtonMenuSidebar
            title="Cadangan"
            icon={<AiOutlineEllipsis className="h-4 w-auto" />}
            status={pathname === "/kas-bank-cadangan" ? true : false}
            onClick={() => handleClick("/coming-soon")}
            tagRight="7%"
            tagRightColor="bg-transparent text-gray-500"
          />
          <ButtonMenuSidebar
            title="Pengurus"
            icon={<AiOutlineEllipsis className="h-4 w-auto" />}
            status={pathname === "/kas-bank-pengurus" ? true : false}
            onClick={() => handleClick("/coming-soon")}
            tagRight="7,5%"
            tagRightColor="bg-transparent text-gray-500"
          />
          <ButtonMenuSidebar
            title="Pengelola"
            icon={<AiOutlineEllipsis className="h-4 w-auto" />}
            status={pathname === "/kas-bank-pengelola" ? true : false}
            onClick={() => handleClick("/coming-soon")}
            tagRight="5%"
            tagRightColor="bg-transparent text-gray-500"
          />
          <ButtonMenuSidebar
            title="Pendidikan"
            icon={<AiOutlineEllipsis className="h-4 w-auto" />}
            status={pathname === "/kas-bank-pendidikan" ? true : false}
            onClick={() => handleClick("/coming-soon")}
            tagRight="5%"
            tagRightColor="bg-transparent text-gray-500"
          />
          <ButtonMenuSidebar
            title="Pengembangan"
            icon={<AiOutlineEllipsis className="h-4 w-auto" />}
            status={pathname === "/kas-bank-pengembangan" ? true : false}
            onClick={() => handleClick("/coming-soon")}
            tagRight="8%"
            tagRightColor="bg-transparent text-gray-500"
          />
          <ButtonMenuSidebar
            title="Sosial"
            icon={<AiOutlineEllipsis className="h-4 w-auto" />}
            status={pathname === "/kas-bank-sosial" ? true : false}
            onClick={() => handleClick("/coming-soon")}
            tagRight="2,5%"
            tagRightColor="bg-transparent text-gray-500"
          />
          <ButtonMenuSidebar
            title="Kontribusi organisasi"
            icon={<AiOutlineEllipsis className="h-4 w-auto" />}
            status={
              pathname === "/kas-bank-kontribusi-organisasi" ? true : false
            }
            onClick={() => handleClick("/coming-soon")}
            tagRight="15%"
            tagRightColor="bg-transparent text-gray-500"
          />
          <ButtonMenuSidebar
            title="Jasa anggota"
            icon={<AiOutlineEllipsis className="h-4 w-auto" />}
            status={pathname === "/kas-bank-jasa-anggota" ? true : false}
            onClick={() => handleClick("/coming-soon")}
            tagRight="50%"
            tagRightColor="bg-transparent text-gray-500"
          />
        </DropdownMenu>

        <DropdownMenu
          title="Pembayaran"
          icon={<AiFillCreditCard className="h-5 w-auto" />}
        >
          <ButtonMenuSidebar
            title="Payroll"
            icon={<AiTwotoneStar className="h-4 w-auto" />}
            status={pathname === "/pembayaran-payroll" ? true : false}
            onClick={() => handleClick("/coming-soon")}
          />
          <ButtonMenuSidebar
            title="Billings"
            icon={<AiTwotoneStar className="h-4 w-auto" />}
            status={pathname === "/pembayaran-billings" ? true : false}
            onClick={() => handleClick("/coming-soon")}
          />
        </DropdownMenu>

        <ButtonMenuSidebar
          title="Laporan"
          icon={<AiOutlineProject className="h-5 w-auto" />}
          status={pathname === "/laporan" ? true : false}
          onClick={() => handleClick("/coming-soon")}
        />
        <DropdownMenu
          title="Management Assets"
          icon={<AiOutlineFileProtect className="h-5 w-auto" />}
        >
          <ButtonMenuSidebar
            title="Assets Pusat"
            icon={<AiOutlineRadiusBottomright className="h-5 w-auto" />}
            status={pathname === "/management-assets" ? true : false}
            onClick={() => handleClick("/management-assets")}
          />
          <ButtonMenuSidebar
            title="Assets Cabang"
            icon={<AiOutlineRadiusSetting className="h-5 w-auto" />}
            status={pathname === "/management-assets/cabang" ? true : false}
            onClick={() => handleClick("/management-assets/cabang")}
          />
        </DropdownMenu>
        <ButtonMenuSidebar
          title="Ticket / Chat"
          icon={<AiOutlineMessage className="h-5 w-auto" />}
          status={pathname === "/ticket" ? true : false}
          onClick={() => handleClick("/ticket")}
          tagRight={isSuccessWaiting && dataWaiting.data.length}
        />
        <ButtonMenuSidebar
          title="User Management"
          icon={<AiOutlineUserSwitch className="h-5 w-auto" />}
          status={pathname === "/user-management" ? true : false}
          onClick={() => handleClick("/user-management")}
        />
        <ButtonMenuSidebar
          title="settings"
          icon={<AiTwotoneSetting className="h-5 w-auto" />}
          status={pathname === "/settings" ? true : false}
          onClick={() => handleClick("/settings")}
        />
        <ButtonMenuSidebar
          title="Logout"
          icon={<AiOutlinePoweroff className="h-5 w-auto" />}
          onClick={() => logout()}
        />
      </div>
    </div>
  );
};
