import React from "react";

export const ButtonMenuSidebar = ({
  title,
  onClick,
  icon,
  tagRight,
  status,
  tagRightColor = "bg-gray-800 text-gray-100",
}) => {
  return (
    <div className="my-2">
      <button
        className={`text-left flex items-center gap-2 rounded py-2 px-2 text-gray-500 hover:text-yellow-500 w-full hover:bg-gray-800 hover:bg-opacity-50` + (status ? " text-yellow-500 hover:text-gray-500 bg-gray-800 bg-opacity-50" : "")}
        onClick={onClick}
      >
        {icon && icon}
        <div className={`${tagRight && "flex justify-between w-full"}`}>
          <span>{title}</span>
          {tagRight ? (
            <span className={`${tagRightColor} p-1 rounded text-xs`}>
              {tagRight}
            </span>
          ) : null}
        </div>
      </button>
    </div>
  );
};
