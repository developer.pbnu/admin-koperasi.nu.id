import React, { useState } from "react";
import { BiChevronDown } from "react-icons/bi";

export const DropdownMenu = ({ title, icon, children }) => {
  const [active, setActive] = useState(false);

  const handleClick = () => {
    setActive(!active);
  };
  return (
    <>
      <div className="my-2">
        <button
          className={`text-left flex items-center gap-2 rounded py-2 px-2 w-full text-gray-500  hover:bg-gray-800 hover:bg-opacity-50 hover:text-yellow-500 ${
            active ? "bg-gray-800 bg-opacity-50" : "bg-gray-800"
          })`}
          onClick={handleClick}
        >
          {icon && icon}
          <div className="flex justify-between w-full">
            <span>{title}</span>
            <BiChevronDown className="h-5 w-auto" />
          </div>
        </button>
      </div>
      {active && <div className="pl-4">{children}</div>}
    </>
  );
};
